import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Comic from "../components/Comic.vue";
import ComicsList from "../components/ComicsList.vue";
import Character from "../components/Character.vue";
import CharactersList from "../components/CharactersList.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      title: "Accueil"
    }
  },
  {
    path: "/comics/:searchValue",
    name: "ComicsList",
    component: ComicsList,
    props: true
  },
  {
    path: "/comic/:id",
    name: "Comic",
    component: Comic,
    props: true
  },
  {
    path: "/characters/:searchValue",
    name: "CharactersList",
    component: CharactersList,
    props: true,
    meta: {
      title: "Liste des personnages"
    }
  },
  {
    path: "/character/:name",
    name: "Character",
    component: Character,
    props: true
  }
];

const router = new VueRouter({
  routes
});

export default router;
